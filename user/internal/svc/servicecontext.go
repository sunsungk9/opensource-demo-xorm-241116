package svc

import (
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/user/internal/config"
	_ "gitee.com/opengauss/openGauss-connector-go-pq"

	"xorm.io/xorm"
)

type ServiceContext struct {
	Config config.Config
	DB     *xorm.Engine
}

func NewServiceContext(c config.Config) *ServiceContext {
	db, err := xorm.NewEngine("postgres", c.DSN)
	if err != nil {
		panic(err)
	}

	return &ServiceContext{
		Config: c,
		DB:     db,
	}
}
