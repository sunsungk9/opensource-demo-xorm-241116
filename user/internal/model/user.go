package model

import (
	"time"
)

type User struct {
	ID           uint       `xorm:"'id' pk autoincr"` // Standard field for the primary key
	Name         string     // 一个常规字符串字段
	Email        *string    // 一个指向字符串的指针, allowing for null values
	Age          uint8      // 一个未签名的8位整数
	Birthday     *time.Time // 生日
	MemberNumber string     // 会员编号
	ActivatedAt  *time.Time `xorm:"activated_at"`               // 激活时间
	CreatedAt    time.Time  `xorm:"created_at default 'now()'"` // 创建时间
	UpdatedAt    *time.Time `xorm:"updated_at"`                 // 最后一次更新时间
}

func (u *User) TableName() string {
	return "users"
}
