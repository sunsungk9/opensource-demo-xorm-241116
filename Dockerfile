FROM golang:alpine AS builder

LABEL stage=gobuilder

ENV CGO_ENABLED=0
ENV GOPROXY=https://goproxy.cn,direct
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories

RUN apk update --no-cache && apk add --no-cache tzdata

WORKDIR /build

COPY . .
RUN go mod tidy
RUN go build -ldflags="-s -w" -o user/user_app user/user.go


FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo/Asia/Shanghai /usr/share/zoneinfo/Asia/Shanghai
ENV TZ=Asia/Shanghai
ENV DSN="host=192.168.0.250 user=root password=ss@111111 dbname=postgres port=8000 sslmode=disable TimeZone=Asia/Shanghai"

WORKDIR /app
COPY --from=builder /build/user/etc /app/etc
COPY --from=builder /build/user/user_app /app/user_app

EXPOSE 8888

CMD ["./user_app"]
